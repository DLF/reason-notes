# My notes on starting with Reason (and Revery)

## Links

### Reason Basics
* [Official Reason docs @ github.io](https://reasonml.github.io/)
* [reasonml.org](https://reasonml.org/)
  Provides basically the same information as the official docs @ github.io.
  Seems pretty redundant but tries to consolidate documentation from different places.
  So, there is some additional stuff but nothing of that seems relevant for me.
* [reason-native.com](https://reason-native.com/)
  Documents five reason libs for native projects and gives some clues on how setting
  up a native reason project might work.
  However, it also doesn’t tell how to setup a native reason project from scratch
  but starts with pulling an existing project.
* [Esy Documentation](https://esy.sh/docs/en/getting-started.html)
  Incomplete docs of Esy. Seem to provide some information about this integrates 
  with Dune and the basics of Esy.
* [Native Hello World with ocamlc](https://dev.to/idkjs/hello-world-with-ocaml-and-reasonml-406b)
  Block post. Short, but helpful to get minimal reason program running with a minimal tool chain.

### Revery Basics
* [Revery quick start @ GitHub](https://github.com/revery-ui/revery-quick-start)
  Gives a short working example but unfortunately no hint how to set up the initial project.
* [Revery Docs](https://www.outrunlabs.com/revery/api/revery/)

### OCaml
* [Documentation](https://ocaml.org/releases/4.10/htmlman/)
* [OPAM](https://opam.ocaml.org/) with documentation
* [Dune Overview](https://dune.readthedocs.io/en/latest/overview.html)
  Referred from reason-native.com, this stuff might help to clarify how 
  a native react project can be set up. The docs don’t mention esy.
* Some explanations about [libraries and OCaml apps with Dune], with a quick explanation of interface files.


## Idea for how to start
1. By using the reason-native hello-world and the Dune docs, create my own hello-world and build it with dune.
2. Make the hello-world print a super-simple calendar of the current month to the terminal.
   I guess & hope this will introduce some dependencies to other libs...
3. Build that thing with Esy. Make esy install all deps, etc.
4. Write some unit tests for that “hello-calendar”. Make that test suite runnable with Esy or Done or both or whatever is meant for that purpose.
5. Some tweaks: Add color output, show calendar weeks, support some command line args for showing multiple months and any specific month.

## Understanding the build env

Let’s use a small but reason sytax specific hello-world, based on ReasonML’s teaser example as `hello.re`:

```reason
type tree = Leaf | Node(int, tree, tree);

let rec sum = (item) => {
  switch (item) {
  | Leaf => 0
  | Node(value, left, right) => value + sum(left) + sum(right);
  }
};

let myTree =
  Node(
    1,
    Node(2, Node(4, Leaf, Leaf), Node(6, Leaf, Leaf)),
    Node(3, Node(5, Leaf, Leaf), Node(7, Leaf, Leaf))
  );


print_endline("The sum of the tree is " ++ string_of_int(sum(myTree)));
```

### Transpiling to OCaml
If `bs-platform` has been installed (via npm), there will be some `refmt` executable somewhere.
Starting `refmt` with the `hello.re` as argument prints out the OCaml equivalent to the small reason program:
```bash
/usr/lib/node_modules/bs-platform/linux/refmt.exe hello.re
```
---
Note: `fmt` seems to be a term for “formatter” in OCaml. I saw `dune` files containing a statement about a required version of `fmt`.
I guess `refmt` just means “reason-formatter” and that – in principle – other formatter could be used to
add more OCaml syntaxs.
---

### Compiling to OCaml Bytecode
The tiny reason program can be compiles to OCaml bytecode with `ocamlc` like so:
```bash
ocamlc -o hello -pp "/usr/lib/node_modules/bs-platform/linux/refmt.exe -p ml" -impl hello.re
```
`-impl` is neccessary to force OCaml compiling a source file which does not end with a
[default suffix](https://ocaml.org/releases/4.10/htmlman/comp.html#s%3Acomp-overview).
`-o` specifies the output filename which defaults to `a.out`.

`-pp` is does the reason-integration trick by calling the transpiler as a preprocessor for each source file.

---
Open Question:
* How can OCaml and ReasonML files be mixed if the transpiler is used for *all* source files?
* AFAIK, this way of using `refmt` as a transpiler is not the way it works later with Dune, 
  where `regmt` acts as a compiler frontend, compiling Reason directly to the OCaml AST.
---

The resulting `hello` file is OCaml bytecode and marked as executable. It has a shebang invoking the vm:
```
#!/usr/bin/ocamlrun
```

(Infos from [here](https://dev.to/idkjs/hello-world-with-ocaml-and-reasonml-406b).)

### Compiling to Native Code
Compiling to native code works the same way, simply by using OCaml’s to-native compiler:
```
ocamlopt -o hello -pp "/usr/lib/node_modules/bs-platform/linux/refmt.exe -p ml" -impl hello.re
```
    
Of couse, there is no shebang in the output file anymore. 
Unlike OCaml bytecode, to-native compiled programs can also not be used in the REPL (interactive session).

This example takes around 20 KBytes as bytecode and about 367 KBytes as native program.

## Built Files
Compiling that single file in the former examples produces `hello`*x* files with the following extensions:
* `.cmo`: Compiled OCaml object file
* `.cmi`: Compiles OCaml interface file
* `.cmx`: Extra information for linking and optimization

In case the project is under revision control, one may want to ignore those file extensions in the VCS.
`cmo` and `cmi` files are introduced in the OCaml docs in chapter
2.5 Modules and separate compilation
and are the input for the linking step within the build.

`cmx` files are introduces in the OCaml docs at
option “opaque” in 9.2 Options (chapter “Batch Compilation”)
and in chapter
12.1 Overview of the compiler.
It seems they are only produced by the native compiler.

## Dune
Dune is a build system for OCaml and ReasonML and uses `refmt` in the background to transpile or compile (not sure) Reason sources.
Even it provides a sub-command `install`, don’t get confused, this just builds the “public artifacts”, it
does’t install anything. Dune does no “package management”.
Its scope is more like the scope of “make” rather than the scope of Python’s “pip”.

Let’s clean the test directory or create a new one with a copy of the `hello.re` file.

There, create a file `dune` with the following content:
```reason
(executable
 (name hello)
 )
```
Being in the test directory, one can just build the hello executable with `dune build`.

### Troubleshooting: getting Reason in for Dune
Doing this the first time, dune aborts with a message “Program refmt not found in the tree or in PATH”.
Following the proposal to `opam install reason` on the other hand quits with 
“[NOTE] Package reason is already installed (current version is 3.6.0).”. So, that doesn’t work.

The problem was, that `eval $(opam env)` wasn’t invoked for my session.
After doing that manually in the shell, it worked.
That also fits the error phenotype. OPAM found the installtion of `refmt` within `~/.opam`
but without exporting the env variables from `opam env`, no independent program – which dune is – can
use the executables that were installed by dune.

It’s worth to have a look at the
[OPAM FAQs](https://opam.ocaml.org/doc/FAQ.html#Why-does-opam-init-need-to-add-stuff-to-my-init-scripts-why-is-eval-opam-env-needed).

### Output and Built Files
Using dune to build creates more files and directories.

Right in the root directory, a `dune-project` file is created.
Even though this is created by dune, it’s usually put under revision control.
It’s just initially created if not existent but can be used for further
project-wide parameters. See
[https://dune.readthedocs.io/en/latest/dune-files.html#dune-project](the docs).

Dune also creates a `.merlin` file, which seems to be some kind of input for the 
[Merlin](https://github.com/ocaml/merlin) system which supports IDEs for 
developer features. I igore this for now, but I guess it has to be ignored by the VCS also...

The built artifacts are placed in a directory `_build`.
It contains a `log` file of the build process and a sub-directoy `default`,
which contains the resulting executable and a bunch of other files and sub-dirs.
`_build` must be ignored by the VCS.

I’m not sure about the meaning of `default` here, but I guess one can add different build 
configurations or something like that.

The executable produced by Dune is a native one, not a bytecode file.
That can be configured, of course.

## 1st Reason Project, initialized with Dune
Let’s start a test project, “rcal” (reason calendar) and let’s try to use `dune init`.

A `dune init proj rcal` creates a new directory with the followint content:
```
  rcal
├──   bin
│  ├──   dune
│  └──   main.ml
├──   lib
│  └──   dune
├──   rcal.opam
└──   test
   ├──   dune
   └──   rcal.ml
```
I also init a git repo there and add everything.

Calling `dune build` creates a lot of new files, also an executable. The complete directory looks now like
```
  rcal
├──   _build
│  ├──   default
│  │  ├──   bin
│  │  │  ├──   dune
│  │  │  ├──   main.exe
│  │  │  └──   main.ml
│  │  ├──   lib
│  │  │  ├──   dune
│  │  │  ├──   rcal.a
│  │  │  ├──   rcal.cma
│  │  │  ├──   rcal.cmxa
│  │  │  ├──   rcal.cmxs
│  │  │  └──   rcal.ml-gen
│  │  ├──   META.rcal
│  │  ├──   rcal.dune-package
│  │  ├──   rcal.install
│  │  ├──   rcal.opam
│  │  └──   test
│  │     ├──   dune
│  │     ├──   rcal.bc
│  │     ├──   rcal.exe
│  │     └──   rcal.ml
│  ├──   install
│  │  └──   default
│  │     ├──   bin
│  │     │  └──   rcal
│  │     └──   lib
│  │        └──   rcal
│  │           ├──   dune-package
│  │           ├──   META
│  │           └──   opam
│  └──   log
├──   bin
│  ├──   dune
│  └──   main.ml
├──   dune-project
├──   lib
│  └──   dune
├──   rcal.opam
└──   test
   ├──   dune
   └──   rcal.ml
```
I add `dune-project` to the git repo and put the other new things into a `.gitignore`:
```
_build
.merlin
```
### Turning the OCaml source file into a Reason one
That’s trivial now. If `eval $(opam env)` has been done in the shell session,
one can just rename `main.ml` to `main.re` and change the syntax withing the file.
The OCaml
`let () = print_endline "Hello, World!"` just needs some extra parenthesis:
`let () = print_endline ("Hello, World!")`. So far, OCaml syntax looks nicer than Reason... :)

# A first try to implement some Logic
Now it’s getting funny. I have no clue about OCaml, Reason or any functional language in general.
Let’s see if we can print some calendar information.

## Getting a OCaml Library
I guess reason doesn’t have it’s own calendar stuff.
Checking for a OCaml calendar lib in OPAM leads me to 
[this lib](https://github.com/ocaml-community/calendar).

Not updated since long. Not really a good first impression of OCamls eco-system.
The [OPAM page](https://opam.ocaml.org/packages/calendar/) and the GitHub page lead to two different documentations.
The [one linked from OPAM](http://calendar.forge.ocamlcore.org/doc/) is useless.
The [one linked from GitHub](https://ocaml-community.github.io/calendar/calendar/CalendarLib/index.html) has at 
least some API docs. (Ohhh... I miss readthedocs already...)

Hm, let’s try to add the lib to the `bin/dune` file:
```
(executable
 (public_name rcal)
 (name main)
 (libraries rcal calendar))
```
So, `dune build` quits with an error now:
```
File "bin/dune", line 4, characters 17-25:
4 |  (libraries rcal calendar))
                     ^^^^^^^^
Error: Library "calendar" not found.
Hint: try: dune external-lib-deps --missing @@default
```
The hint looks doubtful to me. Let’s instead install it with opam:
```
opam install calendar
```
And now `dune build` works again.

### The Pain of getting the Library to work
When trying to address that library, the compiler always quit with “Unbound module Calendar” (or “calendar”, I tried both).
So, how the hell can I use a library? `ocamlfing list` shows that the library is installed.
I also read that dune uses `ocamlfind printconf path` to look up a library. So dune should be able to find it.

I created a new dune project with the same lib specified in the init-step:
```
dune init proj dune1 --libs base,calendar
```
and tried it from the OCaml source file... same result. Trying `open Base`
(the other library declared in my dune init, just because it was copy-pasted from an example)
works. Well, it actually fails, but for another reason. And I assume that that other fail can
only happen if the open statement was successful.

Note: While trying to use a OCaml lib, I wanted to install `esy`.
To install it globally, I needed to
```
sudo npm install -g --unsafe-perm esy
```

In the end, I figured out that the library is called `CalendarLib` in the sources.
Why is the name of the package to install and the module to open different?
And more important, how can I see that? I haven’t seen that information in the libs doc.
Is it some convention?

Anyway, I have my first working example:
```reason
open CalendarLib

let dow_to_short_name = (dow) => {
  switch (dow) {
  | Date.Sun => "Sun"
  | Date.Mon => "Mon"
  | Date.Tue => "Tue"
  | Date.Wed => "Wed"
  | Date.Thu => "Thu"
  | Date.Fri => "Fri"
  | Date.Sat => "Sat"
  };
};

let () = print_endline ("Hello, World!")
let dow = Date.day_of_week (Date.make (2020, 08, 12))
print_endline (dow_to_short_name(dow))
```
It was also though to figure out what values to use in the pattern matching.
I still havn’t understood why it’s `Date.xyz`. 
The compiler mentioned that the type is `FCalendar.day`.
Whatever, I just accept that for now.

Side question: Is there an easy way to turn a variant value into a string?
My `dow_to_short_name` function really looks ridiculous.

I found helpful code examples on these pages:
* https://searchcode.com/?q=lang%3Aocaml+calendar+day_of_week+Tue&lan=64
* https://rosettacode.org/wiki/Day_of_the_week#OCaml

